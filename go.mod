module gitlab.com/silk-app/loom

go 1.16

require (
	github.com/gocql/gocql v0.0.0-20200131111108-92af2e088537
	github.com/gofiber/fiber/v2 v2.8.0
	github.com/scylladb/gocqlx/v2 v2.4.0
)

replace github.com/gocql/gocql => github.com/scylladb/gocql v1.5.0
